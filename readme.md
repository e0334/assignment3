# E0 334 Assignment 3

The deadline for this assignment is **Wednesday, October 17, 2018**.

In this assignment, you are tasked with implementing a preprocessing and classification pipeline for 2 datasets, namely:

1. The [Large Movie Review Dataset](http://ai.stanford.edu/~amaas/data/sentiment/) (IMDB).
2. [AG's News Topic Classification Dataset](https://github.com/mhjabreel/CharCNN/tree/master/data/ag_news_csv) (News).

For the purposes of this assignment, you are expected to use and compare the following classifiers:

1. Denil _et al._'s [Hierarchical CNN (HCNN)](https://arxiv.org/pdf/1406.3830.pdf)
2. Yang _et al._'s [Hierarchical Attention Networks (HAN)](http://www.aclweb.org/anthology/N16-1174)

The relevant fields in both datasets have been tokenized into sentences and words; the tokenized fields are formatted as nested JSON arrays. You may want to use the `json.loads(...)` function to recover them as Python arrays.

## Evaluation

The metric to be used for both datasets is accuracy.  

## Getting Started

1. Please start by forking this repository (tick the checkbox for `This is a private repository`). This should result in the creation of a new repository under your account.
2. Edit the three Python files to implement your solution. In case you are using Python 3, please update the Shebang (`#!`)  to reflect this.
3. Follow the submission instructions to share your solution with us. **Do not submit a Pull Request, unless you want to suggest a fix.**

## Submission Instructions

In your submission, include a `readme.md` with basic instructions and a `requirements.txt` that includes a complete list of packages (and their corresponding versions) that need to be installed. If you make use of any additional data, such as `nltk` packages, you must download them in your code using `nltk.download(...)`. The packages specified will be installed using `pip install -r requirements.txt`, so do not include your Python version, or anything that may not be found by pip. Detail your results on the validation set, feature engineering and feature selection strategies and any ablation studies you might have performed separately in a `report.pdf` file. It should be possible to run your code on a fresh `virtualenv` after installing the specified packages without any other set-up. We will install the specified packages using `pip`, and then run the four Python scripts to check your solution as follows:

    python imdb-HCNN.py --test
    python imdb-HAN.py --test
    python news-HCNN.py --test
    python news-HAN.py --test

Please include your trained weights and load them automatically. You can use Git LFS to store the weights (up to 1GB).  

To submit your solution, go to `Settings > User and group access` and add `peteykun` with `Read` access to the `Users` section. We will automatically retrieve the latest submission via git.
